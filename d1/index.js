// Express.js
/*
	-Web framework
	-set of components designed to simplify the web dev process
		-Convention over configuration
	-allows devs to focus on the business logic of their app
	-Comes in two forms : optionated and unoptionated
*/

	// opinionated web framework
	/*
		-framework dictates how it should be used by the dev
		-speeds up the startup process of app development
		-enforces best practices for the framework's use case
		-lack of flexibility could be a drawback when app's needs are not aligned with the framework's assumption
	*/

	// Unopinionated web framework

	/*
		-dev dictates how ro use the framework
		-offers flexibility to create an application unbound by any use case
		-no "right way" of structuring an application
		-abundance of options may be overwhelming
	*/


/*
	-used the "require" directive to load the express module/package
	-A "module" is a software component or part of a program that contains one or more routines
	-This is used to get the contents of the package to be used by our application
	-It also allows us to access methods and functions that will allow us to easily create a server

*/


	const express = require("express");

	/*
		--create an applicationb using express
		-This creates an express application and stores theis in a constant called app
		-In layman's term, app is our server
	*/

	const app = express();

	// For our application server to run, we need a port to listen to
	const port = 3000;

	// Middlewares
	/*
		-set up for allowing the server to handle data from requests
		-Allows your app to read json data
		-Methods used from express.js are middleware
		-Middleware is a layer of software that enables interaction and transmission of information between assorted application
	*/

	app.use(express.json())

	/*
		-allows your app to read data from forms
		-By default, information received from the URL can only be received as a string ot array
		-By applying the option of "extended.true", we are allowed to receive information in other data types such as an object through our application
	*/

	app.use(express.urlencoded({extended: true}));

// Routes
/*
	-express has methods corresponding to each HTTP method
	-The full base URL for our local application for this route will be at "http://localhost:3000"

*/
// Returns simple message
/*
	-This route expects to receive a GET request at the base URI
	POSTMAN:
	url: http://localhost:3000/
	method: GET
*/

app.get('/', (request, response) => {
	response.send('Hello World')
})

// Return simple message
/*
	URI: /hello
	method: GET

	POSTMAN:
		url: http://localhost:3000/hello

*/
app.get('/hello', (req, res) => {
	res.send('Hello from the "/hello" endpoint')
})


// Return simple greeting
/*
	URI: /hello
	method: POST

	POSTMAN:
		url: http://localhost:3000/hello
		method: POST
		body: raw + json
		{
		"firstName": "John",
		"lastName": "Olivera"

		}
*/

app.post('/hello', (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}! This is from the "/hello" endpoint but a post method`)
})

let users = []

// Register user route

/*
	-this route expects to receive a POST request at the URI "/register"
	-This will create a users object in the "users" variable that mirror a real world registration process

	URI: /hello
	method: POST

	POSTMAN:
	url: http://localhost:3000/register
	method: POST
	body: raw + JSON
		{
		"username": "John",
		"password": ""
		}

*/


	app.post('/register', (request, response) => {

		if (request.body.username !== '' && request.body.password !== '') {
			users.push(request.body)
			response.send(`user ${request.body.username} successfully registered!`)
			console.log(request.body)
		}else {
			response.send('Please input BOTH username and password')
		}
	})



	// Change password route
	/*
		-this route expects to return a PUT request at the URI "/change-password"
		-this will update the password of a user that matches the information provided in the client/postman

		URI: /change-password
		method: PUT

		POSTMAN:
		url: http://localhost:3000/change-password
		method: PUT
		body: raw +json
			{
				"username": "John",
				"password": "abc123"
			}

	*/

	app.use("/change-password", (request, response) => {
		// create a variable to store the message to be sent back to the client/Postman
		let message;
		console.log("works after message")
		// create a for loop through the  elements of the "users" array
		for (let i=0; i < users.length; i++) {

			// if the username provided in the client/postman and the username of the current object in the loop is the same
			console.log('works before if')
			if (request.body.username == users[i].username) {

				// Changes the password of the user found by the loop into the password provided in the client/postman
				users[i].password = request.body.password


				// changes the message to be sent if the password has been updated
				message = `User ${request.body.username}'s password has been updated`


				// Breaks out of the loop once a user that matches the username provided in the client/postman is found
				break;
				
				// if no user is found, else

			}else {
				message = "User does not exist"
			}
		}

		console.log('works before response')
		response.send(message);
	})


	/*
		-tells our server to listen to the port
		-if the port is accessed, we can run the server
		-Returns a message to confirm that the server is running in the terminal

	*/
	app.listen(port, () => console.log(`Server running at port ${port}`));